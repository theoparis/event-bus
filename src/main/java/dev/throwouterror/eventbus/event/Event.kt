package dev.throwouterror.eventbus.event

import com.google.gson.JsonObject

/**
 * The Event class used as a base for other events
 */
open class Event(type: String, parameters: JsonObject = JsonObject()) {
    var type: String = type
        private set
    var parameters: JsonObject
        private set

    init {
        this.parameters = parameters
    }

    /**
     * isCancelled check if the events has been cancelled, will stop the [dev.throwouterror.eventbus.SimpleEventManager] from firing it to lower priority handlers.
     * @return isCancelled if the events is cancelled
     */
    var isCancelled = false
}
